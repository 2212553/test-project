public class Positive{

    public static int sum(int[] arr){
        int sum = 0;
        for(int num:arr){
            if(num>0){
                sum+=num;
            }
        }
        return sum;
    }

}