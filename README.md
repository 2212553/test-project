public class StringSplit {
    public static String[] solution(String s) {
        //Write your code here
        int i = 0;
        int stringLength = s.length();
        int numberOfElements = stringLength/2;
        
        if (stringLength%2 != 0) {
            numberOfElements++;
        }
        String[] results = new String[numberOfElements];
        
        for(int k = 0; k<numberOfElements; k++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(s.charAt(i));
            i++;
          if (i>=s.length()) {
            stringBuilder.append('_');
          } else {
            stringBuilder.append(s.charAt(i));
            i++;
          }
            results[k]=stringBuilder.toString();
        }
        return results;
    }
}


public class Kata {
    public static String createPhoneNumber(int[] numbers) {
        // Your code here!
        StringBuilder sb= new StringBuilder();
        sb.append('(');
        sb.append(numbers[0]);
        sb.append(numbers[1]);
        sb.append(numbers[2]);
        sb.append(") ");
        for (int i = 3; i <= 5; i++) {
            sb.append(numbers[i]);
        }
        sb.append('-');
        for (int i = 6; i <= 9; i++) {
            sb.append(numbers[i]);
        }
        return sb.toString();
    }
}

import java.lang.StringBuilder;
class Solution{
    static String toCamelCase(String s){
        if(s.length()==0){
            return "";
        }
        StringBuilder outputSB = new StringBuilder();
        if(Character.isLetter(s.charAt(0))) {
            outputSB.append(s.charAt(0));
        }
        boolean prevIsDelim=false;
        for(int i=1;i<s.length();i++){
            char c=s.charAt(i);
            if(c=='-'||c=='_'){
                prevIsDelim=true;
                continue;
            }
            if(!Character.isLetter(c)){
                continue;
            }
            if(prevIsDelim){
                outputSB.append(Character.toUpperCase(c));
            }else{
                outputSB.append(Character.toLowerCase(c));
            }
            prevIsDelim=false;
        }
        return outputSB.toString();
    }
}


public class Kata
    {
        public static int opposite(int number)
        {
             return number *= -1;
        }
    }


import java.util.HashSet;

public class CountingDuplicates {
    public static int duplicateCount(String text) {
        // Write your code here
        HashSet<Character> charSet = new HashSet();
        HashSet<Character> repeatedCharSet = new HashSet();
        for(int i=0;i<text.length();i++){
            char currChar=Character.toLowerCase(text.charAt(i));
            if(charSet.contains(currChar)){
                repeatedCharSet.add(currChar);
            }
            charSet.add(currChar);
        }
        return repeatedCharSet.size();
    }

    
}

public class Kata {
    public static final String generateShape(int n) {
     StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                sb.append("+");
            }
            sb.append("\n");
        }
        return sb.toString().trim();
    }
}

public class Kata
    {
        public static int opposite(int number)
        {
             return number *= -1;
        }
    }


 public class Snail {
    public static int[] snail(int[][] array) {
        int rows=array.length;
        // enjoy
        if(array.length<1||array[0].length<1){
            return new int[0];
        }else {
            return arrayOfOuterSquare(rows, 0, array);
        }
    }
 
    private static int[] arrayOfOuterSquare(int size, int offset, int[][] twoDArray) {
        int[] array = new int[size * size];
        short index = 0;
        int row = offset;
        int column = offset;
        if (size > 0) {
            if (size == 1) {
                array[index] = twoDArray[row][column];
            } else {
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    column++;
                } while (column < size - 1 + offset);
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    row++;
                } while (row < size - 1 + offset);
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    column--;
                } while (column > offset);
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    row--;
                } while (row > offset);
                int[] arrayLatter = arrayOfOuterSquare(size - 2, offset + 1, twoDArray);
                for (short i = 0; i < arrayLatter.length; i++) {
                    array[i + index] = arrayLatter[i];
                }
            }
        }
        return array;
    }
}

